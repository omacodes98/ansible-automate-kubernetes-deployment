# Ansible - Automate Kubernetes Deployment 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Create EKS cluster with Terraform 

* Write Ansible Play to deploy application in a new K8s namespace 

## Technologies Used 

* Ansible 

* Terraform 

* Kubernetes 

* AWS EKS 

* Python 

* Linux 

## Steps 

Step 1: Create eks cluster with terraform 

[Automating EKS cluster with terraform](/images/01_create_eks_cluster_with_terraform.png)

note: the terraform configuration file i used is from an old project which will be referenced below in reference section

Step 2: terraform apply to execute EKS cluster 

    terraform apply 

[terraform apply](/images/02_terraform_apply.png)

[eks cluster on aws](/images/03_eks_cluster_on_aws_console.png)

Step 3: Update kubeconfig to have updated cluster 

    aws eks update-kubeconfig --name myapp-eks-cluster --region eu-west-2 

[Update kubeconfig](/images/04_update_kubeconfig_to_have_updated_cluster.png)

Step 4: Create a file for playbook for configuration of deployment to kubernetes 

[Created playbook file](/images/05_create_a_file_for_playbook_for_configuration_of_deployment_to_kubernetes.png)

Step 5: Create first play and give it local host as a host 

[Creating 1st play with name and host](/images/06_create_1st_play_and_give_it_local_host_as_a_host.png)

Step 6: Give play a task using module k8s with following attributes name, api_version, kind, state and kubeconfig_path 

[1st play 1st task module k8](/images/07_give_play_a_task_using_module_k8s_with_following_attributes_name_api_version_kind_state_and_kubeconfig_path.png)

Step 7: Install the following:
  - openshift
  - PyYAML 
  - Jsonpatch 

[Documentation](/images/08_documentation_states_that_we_need_all_this_installed.png)
[Openshift install](/images/09_installing_openshift.png)
[PyYAML install](/images/10_installing_PyYAML.png)
[Jsonpatch install](/images/11_installing_jsonpatch.png)

Step 8: Test ansible playbook 

[ansible playbook test success](/images/12_run_ansible_playbook_success.png)
[active namespace](/images/13_active_name_space.png)

Step 9: Create second task for 1st play to deploy nginx, name task and use attribute:
  - src: path of k8s deployment file 
  - state: set as present
  - kubeconfig: set to path of kube config 
  - namespace: set to namespace name that was created in first task 

[second task](/images/14_create_2nd_task_to_deploy_nginx_name_task_and_use_attribute_src_to_give_path_of_k8s_deployment_file_attribute_state_set_as_present_attribute_kubeconfig_set_to_path_of_kube_config_and_attribute_namespace_set_to_namespace_name_that_was_created_in_1t.png)

Step 10: Test playbook 
 
    ansible-playbook -i host deploy-to-k8s.yaml

[Test playbook 10](/images/15_playbook_ran_successful.png)

Step 11: Check components in cluster 

    kubectl get ns my-app deployment 
    kubectl get svc -n my-app
    kubectl get deployment -n my-app

[Components in cluster](/images/16_components_in_cluster.png)

Step 12: To make file simpler rather than using kubeconfig attribute in everytask you can export it as an environmental variable 

    K8S_AUTH_KUBECONFIG=~/.kube/config 

[export env](/images/17_to_make_file_simpler_rather_than_using_kubeconfig_attribute_in_every_task_you_can_export_it_as_an_environmental_variable.png)

Step 13: Test playbook 
 
    ansible-playbook -i host deploy-to-k8s.yaml

[Test playbook 13](/images/18_success.png)

## Installation

    brew install ansible 

## Usage 

    ansible-playbook -i host deploy-docker.yaml 

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/ansible-automate-kubernetes-deployment.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/ansible-automate-kubernetes-deployment

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.